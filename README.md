# Push Service

The push service is used to push notifications (who would have guessed that?).
The user provides a push subscription to the service which is later used to push updates to the client even if the application is not open.

## Documentation

Production: https://connect.kominal.com/push-service/api-docs
Test: https://connect-test.kominal.com/push-service/api-docs
