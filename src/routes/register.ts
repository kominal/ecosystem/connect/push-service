import { verifyParameter } from '@kominal/service-util/helper/util';
import { PushSubscriptionDatabase } from '@kominal/connect-models/pushsubscription/pushsubscription.database';
import Router from '@kominal/service-util/helper/router';

const router = new Router();

/**
 * Registers a new push subscription
 * @group Protected
 * @security JWT
 * @route POST /register
 * @consumes application/json
 * @produces application/json
 * @param {string} pushSubscription.body.required - The pushSubscription object
 * @param {string} device.body.required - The device identifier encrypted with the master encryption key
 * @returns {void} 200 - The push subscription was added
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/', async (req, res, userId) => {
	const { token, device } = req.body;
	verifyParameter(token, device);
	await PushSubscriptionDatabase.updateOne(
		{ userId, token: JSON.stringify(token) },
		{
			device,
			created: Date.now(),
		},
		{ upsert: true }
	);
	res.status(200).send();
});

export default router.getExpressRouter();
