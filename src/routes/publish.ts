import webpush from 'web-push';
import Router from '@kominal/service-util/helper/router';
import { verifyParameter } from '@kominal/service-util/helper/util';
import { VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY } from '@kominal/service-util/helper/environment';
import { PushSubscriptionDatabase } from '@kominal/connect-models/pushsubscription/pushsubscription.database';

webpush.setVapidDetails('mailto:mail@connect.kominal.com', VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

const router = new Router();

/**
 * Sends a push notification to a user if possible.
 * @group Private
 * @security TOKEN
 * @route POST /publish
 * @consumes application/json
 * @produces application/json
 * @param {string} userIds.body.required - An array of user ids that the event should be delivered to
 * @param {string} pushEvent.body.required - The pushEvent to send
 * @returns {void} 200 - The was delivered if possible
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsRoot('/', async (req, res) => {
	const { userIds, pushEvent } = req.body;
	verifyParameter(userIds, pushEvent);
	const { body, data, actions, renotify, tag } = pushEvent;
	(userIds as string[]).forEach(async (userId) => {
		try {
			const notificationPayload = JSON.stringify({
				notification: {
					title: 'Connect',
					body,
					icon: 'assets/images/logo/logo-512x512.png',
					vibrate: [100, 50, 100],
					data,
					actions,
					renotify,
					tag,
				},
			});
			const subscriptions = await PushSubscriptionDatabase.find({ userId });
			for (const subscription of subscriptions) {
				try {
					const result = await webpush.sendNotification(JSON.parse(subscription.get('token')), notificationPayload);
					if (result.statusCode > 299) {
						await PushSubscriptionDatabase.remove(subscription);
					}
				} catch (error) {
					console.log(error);
				}
			}
		} catch (e) {
			console.log(e);
		}
	});
	res.status(200).send();
});

export default router.getExpressRouter();
