import Router from '@kominal/service-util/helper/router';
import { PushSubscriptionDatabase } from '@kominal/connect-models/pushsubscription/pushsubscription.database';

const router = new Router();

/**
 * Lists all active push subscriptions
 * @group Public
 * @route GET /list
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - A list of push subscriptions
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/', async (req, res, userId) => {
	const subscriptions = (await PushSubscriptionDatabase.find({ userId })).map((pushSubscription) => {
		const { _id, token, device, created } = pushSubscription.toJSON();
		return {
			id: _id,
			token,
			device,
			created,
		};
	});
	res.status(200).send(subscriptions);
});

export default router.getExpressRouter();
