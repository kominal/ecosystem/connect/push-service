import { PushSubscriptionDatabase } from '@kominal/connect-models/pushsubscription/pushsubscription.database';
import { verifyParameter } from '@kominal/service-util/helper/util';
import Router from '@kominal/service-util/helper/router';

const router = new Router();

/**
 * Removes an existing push subscription
 * @group Protected
 * @security JWT
 * @route DELETE /remove/{id}
 * @consumes application/json
 * @produces application/json
 * @param {string} id.path.required - The id of the push subscription
 * @returns {void} 200 - The push subscription was deleted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/:id', async (req, res, userId) => {
	verifyParameter(req.params.id);
	await PushSubscriptionDatabase.deleteMany({ _id: req.params.id, userId });
	res.status(200).send();
});

export default router.getExpressRouter();
