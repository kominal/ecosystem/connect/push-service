import Service from '@kominal/service-util/helper/service';
import register from './routes/register';
import remove from './routes/remove';
import list from './routes/list';
import publish from './routes/publish';
import { SMQHandler } from './handler.smq';

const service = new Service({
	id: 'push-service',
	name: 'Push Service',
	description: 'Manages push notification subscriptions and live updates.',
	jsonLimit: '16mb',
	routes: [register, remove, list, publish],
	database: 'push-service',
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
