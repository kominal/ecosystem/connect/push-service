import { Packet } from '@kominal/service-util/model/packet';
import { info, error } from '@kominal/service-util/helper/log';
import service from '.';
import { PushSubscriptionDatabase } from '@kominal/connect-models/pushsubscription/pushsubscription.database';
import webpush from 'web-push';
import { VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY } from '@kominal/service-util/helper/environment';

webpush.setVapidDetails('mailto:mail@connect.kominal.com', VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

export class SMQHandler {
	async onConnect(): Promise<void> {
		info('Connected to SwarmMQ cluster.');
		service.getSMQClient().subscribe('QUEUE', 'INTERNAL.PUSH');
	}

	async onDisconnect(): Promise<void> {
		error('Lost connection to SwarmMQ cluster.');
	}

	async onMessage(packet: Packet): Promise<void> {
		const { destinationType, destinationName, event } = packet;

		if (destinationType === 'QUEUE' && destinationName === 'INTERNAL.PUSH') {
			if (event) {
				this.onPush(event);
			}
		}
	}

	async onPush(event: { type: string; body?: any }) {
		const { type, body } = event;
		if (!type && !body) {
			return;
		}
		if (type === 'MESSAGE.NEW') {
			const { userIds, groupId } = body;

			for (const userId of userIds) {
				try {
					const notificationPayload = JSON.stringify({
						notification: {
							title: 'Connect',
							body: 'You have a new message!',
							icon: 'assets/images/logo/logo-512x512.png',
							vibrate: [100, 50, 100],
							data: { url: `/connect/group/${groupId}` },
							actions: [{ action: 'open-chat', title: 'Open chat' }],
							renotify: true,
							tag: `MESSAGE_NEW_${groupId}`,
						},
					});
					const subscriptions = await PushSubscriptionDatabase.find({ userId });
					for (const subscription of subscriptions) {
						try {
							const result = await webpush.sendNotification(JSON.parse(subscription.get('token')), notificationPayload);
							if (result.statusCode > 299) {
								await PushSubscriptionDatabase.remove(subscription);
							}
						} catch (error) {
							console.log(error);
						}
					}
				} catch (e) {
					console.log(e);
				}
			}
		}
	}
}
